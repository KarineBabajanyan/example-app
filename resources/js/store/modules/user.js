const state = {
    user:{
        name:'',
        id:'',
        email:'',
    },
    userError:'',
}

// getters
const getters = {
    USER: state => {
        return state.user;
    },
    USERERROR: state => {
        return state.userError
    }
}

// actions
const actions = {
    GET_USER: (context, user) => {
        axios.get('api/users')
        .then(response=>{
            context.commit('SET_USER', response.data.user);
        })
        .catch(function (error) {
            context.commit('SET_USERERROR', error.message)
        });
    },
}

// mutations
const mutations = {
    SET_USER: (state, user) => {
        state.user.name=user.fname + " "+ user.lname;
        state.user.id=user.id;
        state.user.email=user.email;
    },
    SET_USERERROR: (state, error) => {
        state.userError=error
    }
}

export default {
    // namespaced: true,
    state,
    getters,
    actions,
    mutations
}