(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Posts_edit_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Posts/edit.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Posts/edit.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mounted: function mounted() {//
  },
  data: function data() {
    return {
      id: '',
      post: [],
      title: '',
      description: '',
      form: new FormData(),
      image: '',
      error: ''
    };
  },
  methods: {
    onFileChange: function onFileChange(e) {
      console.log(e.target.files[0]);
      this.image = e.target.files[0];
    },
    showData: function showData() {
      var _this = this;

      axios.get('/api/posts/' + this.$route.query.id + '/edit').then(function (response) {
        if (response.status === 200) {
          var that = _this;
          _this.post = response.data.post;
          _this.title = _this.post.title;
          _this.description = _this.post.description;
        }
      })["catch"](function (error) {
        console.log(error);
        _this.error = error.message;
      });
    },
    formSubmit: function formSubmit(e) {
      var _this2 = this;

      console.log(this.title);
      this.form.append('title', this.title);
      this.form.append('description', this.description);
      this.form.append('image', this.image);
      this.form.append("_method", 'patch');
      var config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      };
      axios.post('/api/posts/' + this.post.id, this.form).then(function (response) {
        _this2.$router.push({
          name: 'posts.index'
        });
      })["catch"](function (error) {
        this.error = error.message;
      });
      this.form = new FormData();
    }
  },
  created: function created() {
    this.showData();
  }
});

/***/ }),

/***/ "./resources/js/components/Posts/edit.vue":
/*!************************************************!*\
  !*** ./resources/js/components/Posts/edit.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _edit_vue_vue_type_template_id_08f38871_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit.vue?vue&type=template&id=08f38871&scoped=true& */ "./resources/js/components/Posts/edit.vue?vue&type=template&id=08f38871&scoped=true&");
/* harmony import */ var _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit.vue?vue&type=script&lang=js& */ "./resources/js/components/Posts/edit.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _edit_vue_vue_type_template_id_08f38871_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _edit_vue_vue_type_template_id_08f38871_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "08f38871",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Posts/edit.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Posts/edit.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/Posts/edit.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Posts/edit.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/Posts/edit.vue?vue&type=template&id=08f38871&scoped=true&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Posts/edit.vue?vue&type=template&id=08f38871&scoped=true& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_08f38871_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_08f38871_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_08f38871_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=template&id=08f38871&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Posts/edit.vue?vue&type=template&id=08f38871&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Posts/edit.vue?vue&type=template&id=08f38871&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Posts/edit.vue?vue&type=template&id=08f38871&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row justify-content-center" }, [
      _c(
        "div",
        { staticStyle: { width: "80%", margin: "0 auto" } },
        [
          _vm.error
            ? _c("b-alert", { attrs: { variant: "danger", show: "" } }, [
                _vm._v(_vm._s(_vm.error))
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("div", { staticClass: "large-12 medium-12 small-12 cell" }, [
              _vm.error
                ? _c(
                    "label",
                    {
                      staticClass: "btn btn-outline-secondary disabled",
                      attrs: { "aria-disabled": "true" }
                    },
                    [_vm._v("Files\n                    ")]
                  )
                : _c("label", { staticClass: "btn btn-outline-secondary" }, [
                    _vm._v("Files\n                        "),
                    _c("input", {
                      attrs: {
                        type: "file",
                        id: "files",
                        accept: "image/*",
                        multiple: ""
                      },
                      on: { change: _vm.onFileChange }
                    })
                  ])
            ]),
            _vm._v(" "),
            _c("label", { staticStyle: { width: "100%" } }, [_vm._v("Title:")]),
            _vm._v(" "),
            _vm.error
              ? _c("input", {
                  staticClass: "form-control",
                  attrs: { type: "text", disabled: "" }
                })
              : _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.title,
                      expression: "title"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.title },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.title = $event.target.value
                    }
                  }
                }),
            _vm._v(" "),
            _c("label", [_vm._v("Description:")]),
            _vm._v(" "),
            _vm.error
              ? _c("textarea", {
                  staticClass: "form-control",
                  attrs: { disabled: "" }
                })
              : _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.description,
                      expression: "description"
                    }
                  ],
                  staticClass: "form-control",
                  domProps: { value: _vm.description },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.description = $event.target.value
                    }
                  }
                }),
            _vm._v(" "),
            _vm.error
              ? _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    staticStyle: { "margin-top": "10px" },
                    attrs: { disabled: "" }
                  },
                  [_vm._v("Submit")]
                )
              : _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    staticStyle: { "margin-top": "10px" },
                    on: { click: _vm.formSubmit }
                  },
                  [_vm._v("Submit")]
                )
          ])
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);