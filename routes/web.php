<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'verify' => true,
    'reset' => false,
]);
Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth','verified'])->get('/user', function () {
    return view('spa');
});
// Route::group([], function () {
//     if(auth()->user()){
//     	Route::middleware(['auth','verified'])->get('/', function () {
//         	return view('spa');
//     	});
//     }else{
//     	dd("sdfsd");
//     	Route::get('/', function () {
//         	return view('welcome');
//     	});
//     }
// });
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
