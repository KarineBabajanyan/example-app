<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use File;
use App\Http\Requests\Posts\PostUpdateRequest;
use App\Http\Requests\Posts\PostStoreRequest;
use App\Http\Requests\Posts\PostShowRequest;
use App\Http\Requests\Posts\PostEditRequest;
use App\Http\Requests\Posts\PostDeleteRequest;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $myPosts = auth()->user()->posts()->get();
            return response()->json([
                'myPosts' => $myPosts
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        DB::beginTransaction();

        try {
            $id = auth()->id();
            if($request->image){
                $fileName = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('upload'), $fileName);
            }else{
                 $fileName = "";
            }
            
            $post = Post::create([
                    "title" => $request->title,
                    "description" => $request->description,
                    "user_id" => $id,
                    "image"=>$fileName,
                ]
            );
            
            // Commit Transaction
            DB::commit();

            return response()->json([
                'message' => 'Success'
            ], 200);
        }catch (\Exception $exception){

            // Rollback Transaction
            DB::rollback();

            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PostShowRequest $request)
    {
        try {
            $post = Post::find($id);
            return response()->json([
                'post' => $post,
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, PostEditRequest $request)
    {
        try {
            $post = Post::find($id);
            return response()->json([
                'post' => $post,
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        // Begin Transaction
        DB::beginTransaction();

        try {
            $post = Post::find($id);
            if($request->image){
                $fileName = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('upload'), $fileName);
                $image_path = public_path('upload')."\\".$post->image;  // Value is not URL but directory file path
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $post->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'image'=>$fileName
                ]);
            }else{
                 $post->update([
                    'title' => $request->title,
                    'description' => $request->description
                ]);
            }

            // Commit Transaction
            DB::commit();

            return response()->json([
                'message' => 'Updated successfully'
            ], 200);
        }catch (\Exception $exception){

            // Rollback Transaction
            DB::rollback();

            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, PostDeleteRequest $request)
    {
         // Begin Transaction
        DB::beginTransaction();

        try {
            $post = Post::find($id)->delete();

            // Commit Transaction
            DB::commit();

            return response()->json([
                'message' => 'Success'
            ], 200);
        }catch (\Exception $exception){

            // Rollback Transaction
            DB::rollback();

            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }
    }
}
